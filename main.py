def iou(bbox1: list, bbox2: list) -> float:
    w = min(bbox1[2] + bbox1[3], bbox2[2] + bbox2[3]) - max(bbox1[2], bbox2[2])
    h = min(bbox1[0] + bbox1[1], bbox2[0] + bbox2[1]) - max(bbox1[0], bbox2[0])

    intersection = 0.0 if (w < 0) or (h < 0) else w * h

    union = bbox1[1] * bbox1[3]

    if union == 0:
        return 0
    else:
        return float(intersection / union)

bbox1 = [0, 10, 0, 10]
bbox2 = [0, 10, 1, 10]
bbox3 = [20, 30, 20, 30]
bbox4 = [5, 15, 5, 15]
assert iou(bbox1, bbox1) == 1.0
assert iou(bbox1, bbox2) == 0.9
assert iou(bbox1, bbox3) == 0.0
assert round(iou(bbox1, bbox4), 2) == 0.25
